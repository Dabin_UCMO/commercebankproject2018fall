<!DOCTYPE html>
<html>
<head>
    <title>Show message</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

<#-- Print parameters passed through hash map -->
<p>
    <b>URL Entered:</b> ${url}
</p>
<p>
    <b>Response Header:</b> ${response_header}
</p>
<p>
    <b>Certificate Chain:</b> ${certificate_chain}
</p>
<p>
    <b>Cipher Suite:</b> ${cipher_suite}
</p>
<p>
    <b>Protocols Available (w/ Cipher Suites):</b> ${available_protocols}
</p>
<p>
    <a href="/">Click here to enter another query</a>
</p>

</body>
</html>